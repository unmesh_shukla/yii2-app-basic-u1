<?php

// Merge default config files with local overide files
// DB Connection:
$db     = array_merge(
    require(__DIR__ . '/db.php'),
    require(__DIR__ . '/db-local.php')
);
// Additional Parameters:
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => 'basic',
    'name' => 'Yii2 Basic App',
    'basePath' => dirname(__DIR__),
    'timezone'=>'Europe/Berlin',
    'bootstrap' => [ // {{{ 
        'log',
        'languageSwitcher',
    ], // }}} 
    'aliases' => [ // {{{ 
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@dbbackup' => '@app/data/dbbackup',
    ], // }}} 
    'params' => $params,
    'components' => [
        'db' => $db,
        'authManager' => [ // {{{ 
            'class' => 'dektrium\rbac\components\DbManager',
        ], // }}} 
        'cache' => [ // {{{ 
            'class' => 'yii\caching\FileCache',
        ], // }}} 
        'errorHandler' => [ // {{{ 
            'errorAction' => 'site/error',
        ], // }}} 
        'i18n' => [ // {{{ 
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'lookup*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'lookup' => 'lookup.php',
                    ],
                ],
                'yii2mod.settings' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/settings/messages',
                ],
            ],
        ], // }}}
        'languageSwitcher' => [ // {{{ 
            'class' => 'app\components\LanguageSwitcher',
        ], // }}} 
        'lookup' => [ // {{{
	        'class' => 'app\modules\lookup\models\Lookup',
        ], // }}} 
        'myUtils' => [ // {{{ 
            'class' => 'app\components\myUtils',
        ], // }}} 
        /* 'user' => [ // {{{ 
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ], // }}} */
        'mailer' => [ // {{{ 
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ], // }}} 
        'log' => [ // {{{ 
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ], // }}} 
        'request' => [ // {{{ 
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hHhh98978(/)87)(hHOhoiuOIUopu9E_',
            'csrfParam' => '_csrf-LAUNCHCHECK',
        ], // }}} 
        'session' => [ // {{{ 
            'name' => 'sess-yii2-app-basic',
        ], // }}} 
        'settings' => [ // {{{ 
            'class' => 'yii2mod\settings\components\Settings',
        ], // }}} 
        'urlManager' => [ // {{{ 
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ], // }}} 
        'view' => [ // {{{ 
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views'          => '@app/views/user',
                    // '@dektrium/views/mail/layouts'  => '@app/views/user/mail/layouts',
                ],
            ],
        ], // }}} 
    ],
    'modules' => [ // {{{ 
        'attachments' => [ // {{{ 
            'class' => nemmo\attachments\Module::className(),
            'tempPath' => '@app/data/uploads/temp',
            'storePath' => '@app/data/uploads/store',
            'rules' => [ // Rules according to the FileValidator
                'maxFiles' => $params['uploadMaxFileCount'], // Allow to upload maximum 3 files, default to 3
                //'mimeTypes' => 'image/png', // Only png images
                // 'maxSize' => $params['uploadSingleFileMaxSize'], // 1 MB
            ],
            'tableName' => '{{%attachments}}', // Optional, default to 'attach_file'
        ], // }}} 
        'backuprestore' => [ // {{{ 
            'class' => 'app\modules\backuprestore\Module', 
            //'layout' => '@admin-views/layouts/main'
            'path'=>'@dbbackup',
        ], // }}} 
        'gridview' =>  [ // {{{ 
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ], // }}} 
        'lookup' => [ // {{{ 
            'class' => 'app\modules\lookup\Module',
        ], // }}} 
        'settings' => [ // {{{ 
            'class' => 'yii2mod\settings\Module',
            // Also you can override some controller properties in following way:
            'controllerMap' => [
                'default' => [
                    'class' => 'yii2mod\settings\controllers\DefaultController',
                    'searchClass' => [
                        'class' => 'yii2mod\settings\models\search\SettingSearch',
                        'pageSize' => 25
                    ],
                    /*
                    'modelClass' => 'Your own model class',
                    'indexView' => 'custom path to index view file',
                    'createView' => 'custom path to create view file',
                    'updateView' => 'custom path to update view file',
                     */
                ]
            ]
        ], // }}} 
        'rbac' => [ // {{{ 
            'class'=>'dektrium\rbac\RbacWebModule'
        ], // }}} 
        'user' => [ // {{{ 
            'class' => 'dektrium\user\Module',
            // Admin users: see config/params.php
            'admins' => $params['userAdmins'],
            'enableRegistration' => true, // Don't allow user self registration, we will create admin users manually
            'modelMap' => [
                'User' => 'app\models\User',
            ],
            // 'mailer' => [
            //     'viewPath' => '@app/views/user/mail',
            // ],
        ], // }}}
    ], // }}}   
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    // Add our own templates
    $config['modules']['gii']['generators'] = [ //here
        'crud' => [ // generator name
            'class' => '\app\myTemplates\crud\Generator', // generator class
            'templates' => [ //setting for out templates
                'myCrud' => '@app/myTemplates/crud/default', // template name => path to template
            ]
        ],
        'model' => [ // generator name
            'class' => '\app\myTemplates\model\Generator', // generator class
            'templates' => [ //setting for out templates
                'myModel' => '@app/myTemplates/model/default', // template name => path to template
            ]
        ],
    ];    

}

return $config;
