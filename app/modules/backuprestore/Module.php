<?php
namespace app\modules\backuprestore;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\backuprestore\controllers';
    public $path;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
