<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

class AppParamsForm extends Model
{
    /**
     * @var string application name
     */
    public $contactEmail;

    /**
     * @var string admin email
     */
    public $adminEmail;

    /**
     * @var integer How many news items shall be displayed on the Home page
     */
    public $homeNewsItemsPerPage;

    /**
     * @var integer How many words per news item shall be displayed on the Home page
     */
    public $homeNewsItemWordLimit;

    /**
     * @var string Launch Send Email Subject
     */
    public $launchSendEmailSubject;

    /**
     * @var string Launch Send Email Message
     */
    public $launchSendEmailMessage;

    /**
     * @var string Launch Send Email Bcc
     */
    public $launchSendEmailBcc;

    /**
     * @var string Launch Legend
     */
    public $launchLegend;

    /**
     * @var integer Max. number of characters per comment
     */
    public $actionItemMarketCommentsMaxChars;

    /**
     * @var string Expiration Time of token to update market action item
     */
    public $actionItemMarketTokenExpirationTime;

    /**
     * @var string Expiration Time of token to update market action item again
     */
    public $actionItemMarketTokenUpdateTime;

    /**
     * @var string Define Top X Market groups. Place a label and country codes, separated by >. Separate Top X groups by |.
     */
    public $topXMarkets;

    /**
     * @var string Which Top X Market group to show in report 'Responses by Market'
     */
    public $reportResponsesByMarketShowTopX;

    /**
     * @inheritdoc
     */
    public function rules()
    {;
        return [
            [['contactEmail', 'adminEmail'], 'required'],
            [['contactEmail', 'adminEmail', 'launchSendEmailBcc'], 'email'],
            [['homeNewsItemsPerPage', 'actionItemMarketTokenExpirationTime', 'actionItemMarketTokenUpdateTime'], 'integer', 'min'=>0],
            [['homeNewsItemWordLimit'], 'integer', 'min'=>5],
            [['actionItemMarketCommentsMaxChars'], 'integer', 'min'=>1],
            [['launchLegend'], 'safe'],
            [['launchSendEmailSubject', 'launchSendEmailMessage'], 'safe'],
            [['topXMarkets', 'reportResponsesByMarketShowTopX'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contactEmail'                          => Yii::t('app', 'Contact Email'),
            'adminEmail'                            => Yii::t('app', 'Admin Email'),
            'homeNewsItemsPerPage'                  => Yii::t('app', 'News Items per Page'),
            'homeNewsItemWordLimit'                 => Yii::t('app', 'News Item Word Limit'),
            'launchLegend'                          => Yii::t('app', 'Launch Legend / Glossary'),
            'launchSendEmailSubject'                => Yii::t('app', 'Subject'),
            'launchSendEmailMessage'                => Yii::t('app', 'Message'),
            'launchSendEmailBcc'                    => Yii::t('app', 'Bcc Email'),
            'actionItemMarketCommentsMaxChars'      => Yii::t('app', 'Market Action Item Comments Max. Characters'),
            'actionItemMarketTokenExpirationTime'   => Yii::t('app', 'Market Action Item Expiration Time'),
            'actionItemMarketTokenUpdateTime'       => Yii::t('app', 'Market Action Item Update Time'),
            'topXMarkets'                           => Yii::t('app','Top X Markets'),
            'reportResponsesByMarketShowTopX'       => Yii::t('app','Report Responses By Market - Show Top X'),
        ];
    }
}
