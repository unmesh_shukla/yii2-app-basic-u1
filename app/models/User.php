<?php
namespace app\models;

use Yii;
use dektrium\user\models\User as DektriumUser;

class User extends DektriumUser
{
    // {{{ getMarketsArray
    public function getMarketsArray()
    {
        if(is_array($this->markets))
            return $this->markets;
        return explode(';', $this->markets);
    } // }}} 
    // {{{ setMarketsArray
    public function setMarketsArray($value)
    {
        // die(\yii\helpers\VarDumper::dumpAsString($value, 10, true));
        if(is_array($value)) {
            $this->markets = join(';', $value);
        } else {
            if(trim($value)==='')
                $this->markets = null;
        }

    } // }}} 
    // {{{ scenarios
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        /*
        $scenarios['create'][]   = 'markets';
        $scenarios['update'][]   = 'markets';
        $scenarios['register'][] = 'markets';
         */
        return $scenarios;
    } // }}} 
    // {{{ attributeLabels
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        // $labels['marketsArray'] = Yii::t('app','Markets');
        return $labels;
    } // }}} 
    // {{{ rules
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        // DEBUG \yii\helpers\VarDumper::dump($rules, 10, true);

        // add own rules
        // $rules['markets'] = ['markets', 'string'];
        // $rules['marketsArray'] = ['marketsArray', 'safe'];
        // DEBUG \yii\helpers\VarDumper::dump($rules, 10, true);
        
        return $rules;
    } // }}} 
    // {{{ getDisplayName
    /**
     * Returns a 'speaking' name for thie record instance
     * @return string
     */
    public function getDisplayName()
    {
        $name = $this->username;
        if(!empty($this->profile->name))
            return $this->profile->name . ' ('.$name.')';
        return $name;
    } // }}} 
}
