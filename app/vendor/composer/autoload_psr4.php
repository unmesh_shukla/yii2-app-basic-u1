<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer/src'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui/src'),
    'yii\\httpclient\\' => array($vendorDir . '/yiisoft/yii2-httpclient/src'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii/src'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker/src'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug/src'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap/src'),
    'yii\\authclient\\' => array($vendorDir . '/yiisoft/yii2-authclient/src'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'yii2mod\\settings\\' => array($vendorDir . '/yii2mod/yii2-settings'),
    'yii2mod\\markdown\\' => array($vendorDir . '/yii2mod/yii2-markdown'),
    'yii2mod\\enum\\' => array($vendorDir . '/yii2mod/yii2-enum'),
    'yii2mod\\editable\\' => array($vendorDir . '/yii2mod/yii2-editable'),
    'thrieu\\grid\\' => array($vendorDir . '/thrieu/yii2-grid-view-state'),
    'raoul2000\\bootswatch\\' => array($vendorDir . '/raoul2000/yii2-bootswatch-asset'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'nemmo\\attachments\\' => array($vendorDir . '/nemmo/yii2-attachments/src'),
    'kartik\\select2\\' => array($vendorDir . '/kartik-v/yii2-widget-select2/src'),
    'kartik\\plugins\\fileinput\\' => array($vendorDir . '/kartik-v/bootstrap-fileinput'),
    'kartik\\grid\\' => array($vendorDir . '/kartik-v/yii2-grid/src'),
    'kartik\\file\\' => array($vendorDir . '/kartik-v/yii2-widget-fileinput/src'),
    'kartik\\dialog\\' => array($vendorDir . '/kartik-v/yii2-dialog/src'),
    'kartik\\bs4dropdown\\' => array($vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base/src'),
    'himiklab\\colorbox\\' => array($vendorDir . '/himiklab/yii2-colorbox-widget'),
    'dektrium\\user\\' => array($vendorDir . '/dektrium/yii2-user'),
    'dektrium\\rbac\\' => array($vendorDir . '/dektrium/yii2-rbac'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'bizley\\migration\\' => array($vendorDir . '/bizley/migration/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Contracts\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher-contracts'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src/Prophecy'),
    'Opis\\Closure\\' => array($vendorDir . '/opis/closure/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Codeception\\Extension\\' => array($vendorDir . '/codeception/base/ext'),
    'Codeception\\' => array($vendorDir . '/codeception/base/src/Codeception', $vendorDir . '/codeception/stub/src'),
);
