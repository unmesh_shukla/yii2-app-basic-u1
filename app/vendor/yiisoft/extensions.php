<?php

$vendorDir = dirname(__DIR__);

return array (
  'himiklab/yii2-colorbox-widget' => 
  array (
    'name' => 'himiklab/yii2-colorbox-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@himiklab/colorbox' => $vendorDir . '/himiklab/yii2-colorbox-widget',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'nemmo/yii2-attachments' => 
  array (
    'name' => 'nemmo/yii2-attachments',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@nemmo/attachments' => $vendorDir . '/nemmo/yii2-attachments/src',
    ),
  ),
  'bizley/migration' => 
  array (
    'name' => 'bizley/migration',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@bizley/migration' => $vendorDir . '/bizley/migration/src',
    ),
  ),
  'yii2mod/yii2-enum' => 
  array (
    'name' => 'yii2mod/yii2-enum',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/enum' => $vendorDir . '/yii2mod/yii2-enum',
    ),
  ),
  'yii2mod/yii2-editable' => 
  array (
    'name' => 'yii2mod/yii2-editable',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/editable' => $vendorDir . '/yii2mod/yii2-editable',
    ),
  ),
  'yii2mod/yii2-settings' => 
  array (
    'name' => 'yii2mod/yii2-settings',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/settings' => $vendorDir . '/yii2mod/yii2-settings',
    ),
  ),
  'raoul2000/yii2-bootswatch-asset' => 
  array (
    'name' => 'raoul2000/yii2-bootswatch-asset',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@raoul2000/bootswatch' => $vendorDir . '/raoul2000/yii2-bootswatch-asset',
    ),
  ),
  'thrieu/yii2-grid-view-state' => 
  array (
    'name' => 'thrieu/yii2-grid-view-state',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@thrieu/grid' => $vendorDir . '/thrieu/yii2-grid-view-state',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.2.7.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient/src',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'dektrium/yii2-rbac' => 
  array (
    'name' => 'dektrium/yii2-rbac',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dektrium/rbac' => $vendorDir . '/dektrium/yii2-rbac',
    ),
    'bootstrap' => 'dektrium\\rbac\\Bootstrap',
  ),
  'yii2mod/yii2-markdown' => 
  array (
    'name' => 'yii2mod/yii2-markdown',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii2mod/markdown' => $vendorDir . '/yii2mod/yii2-markdown',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.13.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
);
