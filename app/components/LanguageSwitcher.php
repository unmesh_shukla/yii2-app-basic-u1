<?php
/*
author :: Pitt Phunsanit
website :: http://plusmagi.com
change language by get language=EN, language=TH,...
or select on this widget
*/
 
namespace app\components;
 
use yii;
use yii\base\Component;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\web\Cookie;
 
class LanguageSwitcher extends Widget
{
    /* Available languages */
    public $languages;
 
    public function init()
    {
        if(php_sapi_name() === 'cli')
        {
            return true;
        }
 
        parent::init();
        $this->languages = [
            'en' => Yii::t('app','English'),
            'fr' => Yii::t('app','French'),
            'de' => Yii::t('app','German'),
        ];
        $cookiesRequest     = yii::$app->request->cookies;
        $cookiesResponse    = yii::$app->response->cookies;
        $languageNew = yii::$app->request->get('language');
        if($languageNew)
        {
            if(isset($this->languages[$languageNew]))
            {
                yii::$app->language = $languageNew;
                $cookiesResponse->add(new \yii\web\Cookie([
                    'name' => 'language',
                    'value' => $languageNew
                ]));
            }
        }
        elseif($cookiesRequest->has('language'))
        {
            Yii::$app->language = $cookiesRequest->getValue('language');
        }
 
    }
 
    public function run(){
        $languages = $this->languages;
        $current = '(not set)';
        if(array_key_exists(Yii::$app->language, $languages)) {
            $current = $languages[Yii::$app->language];
            unset($languages[Yii::$app->language]);
        }
 
        $items = [];
        foreach($languages as $code => $language)
        {
            $temp = [];
            $temp['label'] = $language;
            $temp['url'] = Url::current(['language' => $code]);
            array_push($items, $temp);
        }

        echo Yii::t('app', 'Language:').'&nbsp;'; 
        echo ButtonDropdown::widget([
            'label' => $current,
            'dropdown' => [
                'items' => $items,
            ],
        ]);
    }
 
}
