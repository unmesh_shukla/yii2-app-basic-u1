[![gravatar](https://s.gravatar.com/avatar/87473e79ce8d8b84df2f5a291465d42d?s=100)](https://www.diggin-data.de)

# Yii 2 Basic Project Template

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

This is a fork of the offifcial Yii 2 Basic Project Template.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

These modules are included:

* raoul2000/yii2-bootswatch-asset: Use Bootswatch theme in your Yii application with minimum effort
* dektrium/user: Flexible user registration and authentication module for Yii2
* dektrium/rbac: RBAC management module for Yii2
* nemmo/yii2-attachments: Extension for file uploading and attaching to the models
* bizley/migration: Migration generator for Yii 2
* yii2mod/yii2-settings: Yii2 Settings Module
* thrieu/yii2-grid-view-state: Save filters from GridView to session, keep the filter state between pages.

Also it contains a Lookups module and CRUD to manage gridview saved filters.

There are Gii templates for ActiveRecored models and CRUD pages.


[![Latest Stable Version](https://img.shields.io/packagist/v/diggindata/yii2-app-basic.svg)](https://packagist.org/packages/diggindata/yii2-app-basic)
[![Total Downloads](https://img.shields.io/packagist/dt/diggindata/yii2-app-basic.svg)](https://packagist.org/packages/diggindata/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      data/               contains folders for database backups and uploaded files/attachments
      mail/               contains view files for e-mails
      models/             contains model classes
      modules/            contains application modules, currently that are `backuprestore` and `lookup`
      migrations/         contains database migrations to create tables needed
      myTemplates/        contains gii templates for models and CRUD pages
      rbac/               contains an example rule `Author` 
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources
      widgets/            contains widhets, currently `Alert` and `GridView`



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

* see separate file [INSTALL.md](INSTALL.md)

CONFIGURATION
-------------

* see separate file [INSTALL.md](INSTALL.md)

TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2_basic_tests` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run -- --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit -- --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit -- --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.
